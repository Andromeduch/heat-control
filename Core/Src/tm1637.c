/*
 * tm1637.c
 *
 *  Created on: 29 сент. 2019 г.
 *      Author: dima
 */

#include "delay_micros.h"
#include "tm1637.h"

static int8_t TubeTab[] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f, 0x77,0x7c,0x40}; //0~9, A(10), b(11), -(12)

void CLK_HIGH(struct TM1637_t *tm){
	HAL_GPIO_WritePin(tm->clkPort_, tm->clkPin_, GPIO_PIN_SET);
}
void CLK_LOW(struct TM1637_t *tm){
	HAL_GPIO_WritePin(tm->clkPort_, tm->clkPin_, GPIO_PIN_RESET);
}

void DIO_HIGH(struct TM1637_t *tm){
	HAL_GPIO_WritePin(tm->dioPort_, tm->dioPin_, GPIO_PIN_SET);
}
void DIO_LOW(struct TM1637_t *tm){
	HAL_GPIO_WritePin(tm->dioPort_, tm->dioPin_, GPIO_PIN_RESET);
}

GPIO_PinState DIO_READ(struct TM1637_t *tm)
{
	return HAL_GPIO_ReadPin(tm->dioPort_, tm->dioPin_);
}

void writeByte(struct TM1637_t *tm1637, int8_t wr_data)
{
  uint8_t count = 0;

  for(uint8_t i = 0; i < 8; i++)
  {
    CLK_LOW(tm1637);

    if(wr_data & 0x01) DIO_HIGH(tm1637);
    else DIO_LOW(tm1637);

    delay_micros(6);
    wr_data >>= 1;
    delay_micros(6);
    CLK_HIGH(tm1637);
    delay_micros(8);
  }

  CLK_LOW(tm1637);
  delay_micros(6);
  DIO_HIGH(tm1637);
  delay_micros(6);
  CLK_HIGH(tm1637);
  delay_micros(8);

  while(DIO_READ(tm1637))
  {
    count += 1;

    if(count == 200)
    {
    	DIO_LOW(tm1637);
    	count = 0;
    }
  }
}

void start(struct TM1637_t *tm1637)
{
	CLK_HIGH(tm1637);
	delay_micros(6);
	DIO_HIGH(tm1637);
	delay_micros(6);
	DIO_LOW(tm1637);
	delay_micros(6);
	CLK_LOW(tm1637);
}

void stop(struct TM1637_t *tm1637)
{
	CLK_LOW(tm1637);
	delay_micros(6);
	DIO_LOW(tm1637);
	delay_micros(6);
	CLK_HIGH(tm1637);
	delay_micros(6);
	DIO_HIGH(tm1637);
}

void display_mass(struct TM1637_t *tm1637, int8_t DispData[])
{
	int8_t SegData[4];

	for(uint8_t i = 0; i < 4; i++)
	{
		SegData[i] = DispData[i];
	}

	coding_mass(tm1637, SegData);
	start(tm1637);
	writeByte(tm1637, ADDR_AUTO);
	stop(tm1637);
	start(tm1637);
	writeByte(tm1637, 0xc0);

	for(uint8_t i = 0; i < 4; i++)
	{
		writeByte(tm1637, SegData[i]);
	}

	stop(tm1637);
	start(tm1637);
	writeByte(tm1637, tm1637->Cmd_DispCtrl);
	stop(tm1637);
}

void display(struct TM1637_t *tm1637, uint8_t BitAddr, int8_t DispData)
{
	int8_t SegData;

	SegData = coding(tm1637, DispData);
	start(tm1637);
	writeByte(tm1637, ADDR_FIXED);

	stop(tm1637);
	start(tm1637);
	writeByte(tm1637, BitAddr | 0xc0);

	writeByte(tm1637, SegData);
	stop(tm1637);
	start(tm1637);

	writeByte(tm1637, tm1637->Cmd_DispCtrl);
	stop(tm1637);
}

void clearDisplay(struct TM1637_t *tm1637)
{
	display(tm1637, 0x00, 0x7f);
	display(tm1637, 0x01, 0x7f);
	display(tm1637, 0x02, 0x7f);
	display(tm1637, 0x03, 0x7f);
}

void set_brightness(struct TM1637_t *tm1637, uint8_t brightness)
{
	tm1637->Cmd_DispCtrl = 0x88 + brightness;
}

void point(struct TM1637_t *tm1637, uint8_t cmd)
{
	if(cmd == 0) tm1637->point_flag = (~(tm1637->point_flag)) & 0x01;
	else tm1637->point_flag = 1;
}

void coding_mass(struct TM1637_t *tm1637, int8_t DispData[])
{
	uint8_t PointData;

	if(tm1637->point_flag == 1) PointData = 0x80;
	else PointData = 0;

	for(uint8_t i = 0; i < 4; i++)
	{
		if(DispData[i] == 0x7f) DispData[i] = 0x00;
		else DispData[i] = TubeTab[DispData[i]] + PointData;
	}
}

int8_t coding(struct TM1637_t *tm1637, int8_t DispData)
{
	uint8_t PointData;

	if(tm1637->point_flag == 1) PointData = 0x80;
	else PointData = 0;

	if(DispData == 0x7f) DispData = 0x00 + PointData;
	else DispData = TubeTab[DispData] + PointData;

	return DispData;
}
