/*
 * tm1637.h
 *
 *  Created on: 29 сент. 2019 г.
 *      Author: dima
 */

#ifndef TM1637_H_
#define TM1637_H_

#include "main.h"

struct TM1637_t{
	GPIO_TypeDef *clkPort_;
	uint16_t clkPin_;
	GPIO_TypeDef *dioPort_;
	uint16_t dioPin_;
	uint8_t Cmd_DispCtrl;
	uint8_t point_flag;
} ;

void CLK_HIGH(struct TM1637_t *);
void CLK_LOW(struct TM1637_t *);

void DIO_HIGH(struct TM1637_t *);
void DIO_LOW(struct TM1637_t *);

GPIO_PinState DIO_READ(struct TM1637_t *);


//#define CLK_HIGH	(HAL_GPIO_WritePin(TM1637_1_CLK_GPIO_Port, TM1637_1_CLK_Pin, GPIO_PIN_SET))
//#define CLK_LOW		(HAL_GPIO_WritePin(TM1637_1_CLK_GPIO_Port, TM1637_1_CLK_Pin, GPIO_PIN_RESET))
//
//#define DIO_HIGH	(HAL_GPIO_WritePin(TM1637_1_DIO_GPIO_Port, TM1637_1_DIO_Pin, GPIO_PIN_SET))
//#define DIO_LOW		(HAL_GPIO_WritePin(TM1637_1_DIO_GPIO_Port, TM1637_1_DIO_Pin, GPIO_PIN_RESET))
//
//#define DIO_READ	(HAL_GPIO_ReadPin(TM1637_1_DIO_GPIO_Port, TM1637_1_DIO_Pin))

#define ADDR_AUTO  0x40
#define ADDR_FIXED 0x44

#define STARTADDR  0xc0

void writeByte(struct TM1637_t *tm1637, int8_t wr_data);//write 8bit data to tm1637
void start(struct TM1637_t *tm1637);//send start bits
void stop(struct TM1637_t *tm1637); //send stop bits
void display_mass(struct TM1637_t *tm1637, int8_t DispData[]);
void display(struct TM1637_t *tm1637, uint8_t BitAddr,int8_t DispData);
void clearDisplay(struct TM1637_t *tm1637);
void set_brightness(struct TM1637_t *tm1637, uint8_t brightness); //To take effect the next time it displays.
void point(struct TM1637_t *tm1637, uint8_t cmd); //whether to light the clock point ":".To take effect the next time it displays.
void coding_mass(struct TM1637_t *tm1637, int8_t DispData[]);
int8_t coding(struct TM1637_t *tm1637, int8_t DispData);

#endif /* TM1637_H_ */
